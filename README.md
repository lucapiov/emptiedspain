This repo is intended to store data, scripts and visualizations used to dig a little bit into the problem of the emptied Spain (a.k.a. "España vaciada" in Spanish).

The idea is to bring to light some patterns hidden in the changes of population census across the different municipalities of Spain in the period from 1998 to 2018.

The data used here come from INE, the Spanish Statistical Institute (https://www.ine.es/)
